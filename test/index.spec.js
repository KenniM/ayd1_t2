import request from 'supertest';
import app from "../src/app.js";

describe('POST /registro', ()=> {
    let data = {
        id:123456789,
        nombre: 'Kenni',
        apellido: 'Martínez',
        email: 'kr.martinez26@outlook.com',
        fechanac: '11-26-1999',
        contrasenia: 'superduperPassword',
        confcontrasenia: 'superduperPassword'
    }
    test('Esperado statusCode 200', async ()=>{
        
        const response = await request(app).post('/registro').send(data);   
        expect(response.statusCode).toBe(200);
    })

    test('Verificar que el id sea un número entero', async ()=>{
        
        const response = await request(app).post('/registro').send(data);
        expect(response.body.id_valido).toBe(true);
    })

    test('Verificar que la contraseña coincida con la confirmación', async ()=>{
        
        const response = await request(app).post('/registro').send(data);
        expect(response.body.contrasenia_valida).toBe(true);
    })
});

describe('POST /login', ()=> {
    let data = {
        email: 'kr.martinez26@outlook.com',
        contrasenia: 'superduperPassword'
    }
    test('Esperado statusCode 200', async ()=>{
        
        const response = await request(app).post('/login').send(data);   
        expect(response.statusCode).toBe(200);
    })

    test('Verificar que el email sea válido', async ()=>{
        
        const response = await request(app).post('/login').send(data);
        expect(response.body.email_valido).toBe(true);
    })

    test('Verificar que la contraseña no esté vacía', async ()=>{
        
        const response = await request(app).post('/login').send(data);
        expect(response.body.contrasenia_valida).toBe(true);
    })
});

describe('POST /asignacion', ()=> {
    let data = {
        idAsignacion:1234,
        estudiante:201800457,
        curso:'Análisis y Diseño de Sistemas 1',
        seccion:'N',
        dia:'Lunes',
        hora:'11:00'
    }
    test('Verificar que tanto idAsignacion como estudiante sean id de tipo entero', async ()=>{
        
        const response = await request(app).post('/asignacion').send(data);   
        expect(response.body.ids_validos).toBe(true);
    })

    test('Verificar que el dia de la semana sea válido', async ()=>{
        
        const response = await request(app).post('/asignacion').send(data);
        expect(response.body.dia_valido).toBe(true);
    })

    test('Verificar que la hora esté en formato de 24hrs', async ()=>{
        
        const response = await request(app).post('/asignacion').send(data);
        expect(response.body.hora_valida).toBe(true);
    })
});