import Express from "express";

const app = Express()

app.use(Express.json())

app.post('/registro', (req, res) => {
    let { id, nombre, apellido, email, fechanac, contrasenia, confcontrasenia } = req.body;
    let flagId;
    let flagContrasenia;
    flagId=Number.isInteger(id);
    flagContrasenia=contrasenia === confcontrasenia;
    res.status(200).json({id:id,nombre:nombre,apellido:apellido,email:email,fecha_nacimiento:fechanac,contrasenia_valida:flagContrasenia,id_valido:flagId});

});

app.post('/login',(req,res) => {
    let {email,contrasenia} = req.body;
    let re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    let emailFlag = re.test(email);
    let passwordFlag = Boolean(contrasenia != '' | contrasenia != undefined);
    res.status(200).json({email_valido:emailFlag,contrasenia_valida:passwordFlag});
})

app.post('/asignacion', (req,res) =>{
    let dayArray = ["lunes","martes","miercoles","jueves","viernes","sabado","domingo"]
    let {idAsignacion,estudiante,curso,seccion,dia,hora} = req.body;
    let idsFlag = (Number.isInteger(idAsignacion) && Number.isInteger(estudiante));
    let re = /^([01][0-9]|2[0-3]):([0-5][0-9])$/
    let horaFlag = re.test(hora);
    let diaFlag = dayArray.includes(String(dia).toLowerCase());
    res.status(200).json({ids_validos:idsFlag,hora_valida:horaFlag,dia_valido:diaFlag});
})

export default app;